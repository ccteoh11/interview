//
//  WeatherAppDemo99Tests.swift
//  WeatherAppDemo99Tests
//
//  Created by CC Teoh on 22/03/2024.
//

import XCTest
@testable import WeatherAppDemo99

final class WeatherAppDemo99Tests: XCTestCase {
    var viewModel: PostsViewModel!
    var mockNetworkClient: MockNetworkClient!
    
    override func setUpWithError() throws {
        mockNetworkClient = MockNetworkClient()
        viewModel = PostsViewModel(networkClient: mockNetworkClient)
    }
    
    override func tearDownWithError() throws {
        viewModel = nil
        mockNetworkClient = nil
    }
    
    func testFetchPosts_Success() {
        let jsonData = """
            [
                {"id": 1, "title": "Post 1", "body": "Body 1"},
                {"id": 2, "title": "Post 2", "body": "Body 2"}
            ]
            """.data(using: .utf8)!
        mockNetworkClient.data = jsonData
        
        let expectation = XCTestExpectation(description: "Fetch posts")
        
        viewModel.onUpdate = {
            expectation.fulfill()
        }
        
        viewModel.fetchPosts()
        
        wait(for: [expectation], timeout: 5)
        
        XCTAssertEqual(viewModel.getPostsCount(), 2, "Posts should be fetched")
    }
    
    func testFetchPosts_Failure() {
        let mockError = NSError(domain: "MockError", code: 0, userInfo: nil)
        mockNetworkClient.error = mockError
        
        viewModel.fetchPosts()
        
        XCTAssertEqual(viewModel.getPostsCount(), 0, "Posts count should be zero")
    }
}
