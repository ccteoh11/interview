//
//  DownloadTask.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class DownloadTask {
    let url: URL
    var progress: Float = 0.0
    var status: DownloadStatus = .pending
    
    init(url: URL) {
        self.url = url
    }
}

enum DownloadStatus {
    case pending
    case downloading
    case completed
    case failed
}
