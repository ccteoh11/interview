//
//  WeatherData.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

struct WeatherData {
    let temperature: Double
    let conditions: String
    let location: String
}
