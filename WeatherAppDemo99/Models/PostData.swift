//
//  PostData.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

struct Post: Codable {
    let id: Int
    let title: String
    let body: String
}
