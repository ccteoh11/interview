//
//  SeatData.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

struct Seat {
    var id: Int
    var row: Int
    var column: Int
    var isSelected: Bool
}
