//
//  LocalizationFactory.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

struct LocalizationFactory {
    static func localizedString(forKey key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
