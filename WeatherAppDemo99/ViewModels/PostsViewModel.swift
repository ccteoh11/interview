//
//  PostViewModel.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class PostsViewModel {
    private var posts: [Post] = []
    private var currentPage = 1
    private let itemsPerPage = 20
    private var isLoading = false
    private let networkClient: NetworkProtocol
    
    init(networkClient: NetworkProtocol = URLSessionNetworkClient()) {
        self.networkClient = networkClient
    }

    var onUpdate: (() -> Void)?
    
    func fetchPosts() {
        guard !isLoading else { return }
        isLoading = true
        
        let urlString = "https://jsonplaceholder.typicode.com/posts?_page=\(currentPage)&_limit=\(itemsPerPage)"
        guard let url = URL(string: urlString) else { return }
        
        networkClient.fetchData(from: url) { [weak self] result in
            guard let self = self else { return }
            defer { self.isLoading = false }
            
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let newPosts = try decoder.decode([Post].self, from: data)
                    self.posts.append(contentsOf: newPosts)
                    self.onUpdate?()
                } catch {
                    print("Error decoding JSON: \(error)")
                }
            case .failure(let error):
                print("Error fetching posts: \(error)")
            }
        }
    }
    
    func getPostsCount() -> Int {
        return posts.count
    }
    
    func getPost(at index: Int) -> Post {
        return posts[index]
    }
    
    @discardableResult func loadMoreIfNeeded(currentIndex: Int) -> Bool {
        let threshold = posts.count - 5
        if currentIndex >= threshold {
            currentPage += 1
            fetchPosts()
            return true
        } else {
            return false
        }
    }
}
