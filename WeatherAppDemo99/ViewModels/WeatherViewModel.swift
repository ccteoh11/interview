//
//  WeatherViewModel.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class WeatherViewModel {
    var weatherData: WeatherData?
    let weatherService: WeatherAPIService
    
    init(weatherService: WeatherAPIService) {
        self.weatherService = weatherService
    }
    
    func fetchWeatherData(completion: @escaping (WeatherData?) -> Void) {
        weatherService.fetchWeather { result in
            switch result {
            case .success(let data):
                self.weatherData = data
                completion(data)
            case .failure(_):
                completion(nil)
            }
        }
    }
}
