//
//  DownloadViewModel.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class DownloadViewModel: NSObject {
    var downloadTasks: [DownloadTask] = []
    private lazy var backgroundSession: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "com.app.WeatherAppDemo99Tests")
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()

    func startDownloads() {
        for task in downloadTasks {
            downloadTask(task)
        }
    }
    
    private func downloadTask(_ task: DownloadTask) {
        task.status = .downloading
        let downloadTask = backgroundSession.downloadTask(with: task.url)
        downloadTask.resume()
    }
}

extension DownloadViewModel: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let originalRequestURL = downloadTask.originalRequest?.url else { return }
        let fileName = UUID().uuidString + originalRequestURL.lastPathComponent
        let destinationURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(fileName)
        do {
            try FileManager.default.moveItem(at: location, to: destinationURL)
            if let index = downloadTasks.firstIndex(where: { $0.url == originalRequestURL }) {
                downloadTasks[index].status = .completed
            }
        } catch {
            print("Error moving downloaded file: \(error.localizedDescription)")
            if let index = downloadTasks.firstIndex(where: { $0.url == originalRequestURL }) {
                downloadTasks[index].status = .failed
            }
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let originalRequestURL = task.originalRequest?.url else { return }
        if let error = error {
            print("Download task failed: \(error.localizedDescription)")
            if let index = downloadTasks.firstIndex(where: { $0.url == originalRequestURL }) {
                downloadTasks[index].status = .failed
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard let originalRequestURL = downloadTask.originalRequest?.url,
              let index = downloadTasks.firstIndex(where: { $0.url == originalRequestURL }) else { return }
        
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        downloadTasks[index].progress = progress
        print("Download Progress: \(Int(downloadTasks[index].progress * 100))%")
    }

}
