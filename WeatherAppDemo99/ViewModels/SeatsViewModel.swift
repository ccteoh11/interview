//
//  SeatsViewModel.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class SeatSelectionViewModel {
    private let sqliteManager = SQLiteManager.shared
    
    func updateSeatSelection(seat: Seat, isSelected: Bool) {
        sqliteManager.updateSeatSelection(seatId: seat.id, isSelected: isSelected)
    }
    
    func getAllSeats() -> [Seat] {
        return sqliteManager.getAllSeats()
    }
    func createSeatsIfNeeded() {
        if !sqliteManager.seatsExist() {
               var newSeats: [Seat] = []
               let rows = 20
               
               for row in 1...rows {
                   for column in 1...4 {
                       let isSelected = false
                       let seat = Seat(id: 0, row: row, column: column, isSelected: isSelected)
                       newSeats.append(seat)
                   }
               }
               
               sqliteManager.insertSeats(newSeats)
           }
       }
    
}
