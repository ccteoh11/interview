//
//  WeatherDataObserver.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

protocol WeatherDataObserver {
    // Observer protocol methods and properties
    
    func weatherDataDidUpdate(_ weatherData: WeatherData)
}
