//
//  PostsViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

class PostsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = PostsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        viewModel.onUpdate = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.fetchPosts()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getPostsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        
        let post = viewModel.getPost(at: indexPath.row)
        cell.titleLabel?.text = post.title
        cell.bodyLabel?.text = post.body
        
        // Load image asynchronously
        DispatchQueue.global().async {
            if let imageUrl = URL(string: "https://via.placeholder.com/150") {
                do {
                    let imageData = try Data(contentsOf: imageUrl)
                    DispatchQueue.main.async {
                        cell.postImageView?.image = UIImage(data: imageData)
                        cell.setNeedsLayout()
                    }
                } catch {
                    print("Error loading image: \(error)")
                }
            }
        }

        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentIndex = tableView.indexPathsForVisibleRows?.last?.row ?? 0
        viewModel.loadMoreIfNeeded(currentIndex: currentIndex)
    }
}
