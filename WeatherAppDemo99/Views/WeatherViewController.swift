//
//  WeatherViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

class WeatherViewController: UIViewController {
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var viewModel: WeatherViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = WeatherViewModel(weatherService: WeatherAPIService())
        fetchWeatherData()
    }
    
    func fetchWeatherData() {
        viewModel.fetchWeatherData { data in
            DispatchQueue.main.async {
                if let data = data {
                    self.updateUI(with: data)
                } else {
                    self.showAlert(message: "Failed to fetch weather data.")
                }
            }
        }
    }
    
    func updateUI(with data: WeatherData) {
        temperatureLabel.text = "\(data.temperature)°C"
        conditionsLabel.text = data.conditions
        locationLabel.text = data.location
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
