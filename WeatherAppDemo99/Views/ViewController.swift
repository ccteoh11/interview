//
//  ViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tableViewDemoBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: "ToUITableVIewScreen", sender: nil)
    }
    
    @IBAction func collectionViewDemoBtnPressed(_ sender: Any) {
        let seatSelectionVC = SeatSelectionViewController()
        let navigationController = UINavigationController(rootViewController: seatSelectionVC)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func downloadTasksBtnPressed(_ sender: Any) {
        let downloadsListVC = DownloadsListViewController()
        let navigationController = UINavigationController(rootViewController: downloadsListVC)
        present(navigationController, animated: true, completion: nil)

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ToUITableVIewScreen" {
//            let postsVC = segue.destinationViewController as! PostsViewController
//        }
    }
    

}
