//
//  SeatsSelectionViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

class SeatSelectionViewController: UIViewController {
    private var seatSelectionView: SeatSelectionView!
    private var viewModel: SeatSelectionViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        viewModel = SeatSelectionViewModel()
        
        // create seats if empty seats in db
        viewModel.createSeatsIfNeeded()
        let seats = viewModel.getAllSeats()
        seatSelectionView = SeatSelectionView(frame: view.bounds, seats: seats)
        seatSelectionView.delegate = self
        view.addSubview(seatSelectionView)
    }
}

extension SeatSelectionViewController: SeatSelectionDelegate {
    
    // TODO: can use toggle
    func didSelectSeat(_ seat: Seat) {
        viewModel.updateSeatSelection(seat: seat, isSelected: true)
    }
    
    func didDeselectSeat(_ seat: Seat) {
        viewModel.updateSeatSelection(seat: seat, isSelected: false)
    }
}

