//
//  DownloadsListViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

class DownloadsListViewController: UIViewController {
    var viewModel = DownloadViewModel()
    
    private let tableView = UITableView()
    private let startButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setupUI()
        addingDownloadTasks()
    }
    
    private func addingDownloadTasks() {
        // Add download tasks
        let url1 = URL(string: "https://apod.nasa.gov/apod/image/2403/STSCI-MarsPhobosComp3000.jpg")!
        let url2 = URL(string: "https://apod.nasa.gov/apod/image/2403/STSCI-MarsPhobosComp3000.jpg")!
        let url3 = URL(string: "https://apod.nasa.gov/apod/image/2403/STSCI-MarsPhobosComp3000.jpg")!
        let url4 = URL(string: "https://apod.nasa.gov/apod/image/2403/STSCI-MarsPhobosComp3000.jpg")!

        let task1 = DownloadTask(url: url1)
        let task2 = DownloadTask(url: url2)
        let task3 = DownloadTask(url: url3)
        let task4 = DownloadTask(url: url4)

        viewModel.downloadTasks = [task1, task2, task3, task4]
    }
    
    private func setupUI() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        view.addSubview(tableView)
        startButton.translatesAutoresizingMaskIntoConstraints = false
        startButton.setTitle("Start Downloads", for: .normal)
        startButton.setTitleColor(.blue, for: .normal)
        startButton.addTarget(self, action: #selector(startDownloads), for: .touchUpInside)
        view.addSubview(startButton)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            tableView.bottomAnchor.constraint(equalTo: startButton.topAnchor, constant: -20),
            
            startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            startButton.heightAnchor.constraint(equalToConstant: 50),
            startButton.widthAnchor.constraint(equalToConstant: 150),
            startButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
        ])
    }
    
    @objc private func startDownloads() {
        viewModel.startDownloads()
    }
}

extension DownloadsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.downloadTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO: create custom cell add progress bar
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let task = viewModel.downloadTasks[indexPath.row]
        cell.textLabel?.text = task.url.absoluteString        
        return cell
    }
}
