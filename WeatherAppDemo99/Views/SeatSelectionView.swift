//
//  SeatSelectionViewController.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import UIKit

protocol SeatSelectionDelegate: AnyObject {
    // TODO: can use toggle
    func didSelectSeat(_ seat: Seat)
    func didDeselectSeat(_ seat: Seat)
}

class SeatSelectionView: UIView {
    weak var delegate: SeatSelectionDelegate?
    private var collectionView: UICollectionView!
    private var seats: [Seat]
    
    init(frame: CGRect, seats: [Seat]) {
        self.seats = seats
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 50, height: 50)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        addSubview(collectionView)
    }
}

extension SeatSelectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.backgroundColor = seats[indexPath.item].isSelected ? .green : .gray
        return cell
    }
}

extension SeatSelectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        seats[indexPath.item].isSelected = !seats[indexPath.item].isSelected
        if seats[indexPath.item].isSelected {
            delegate?.didSelectSeat(seats[indexPath.item])
        } else {
            delegate?.didDeselectSeat(seats[indexPath.item])
        }
        collectionView.reloadItems(at: [indexPath])
    }
}
