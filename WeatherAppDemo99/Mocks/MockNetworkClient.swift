//
//  MockURLSession.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class MockNetworkClient: NetworkProtocol {
    var data: Data?
    var error: Error?

    func fetchData(from url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else if let data = data {
            completion(.success(data))
        } else {
            fatalError("MockNetworkClient must be configured with either data or error")
        }
    }
}

