//
//  WeatherAPIServices.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation

class WeatherAPIService {
    func fetchWeather(completion: @escaping (Result<WeatherData, Error>) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            let weatherData = WeatherData(temperature: 25.0, conditions: "Sunny", location: "New York")
            completion(.success(weatherData))
        }
    }
}
