//
//  SQLiteManager.swift
//  WeatherAppDemo99
//
//  Created by CC Teoh on 22/03/2024.
//

import Foundation
import SQLite3

class SQLiteManager {
    static let shared = SQLiteManager()
    private var db: OpaquePointer?
    private let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    
    private init() {
        openDatabase()
        createTable()
    }
    
    deinit {
        closeDatabase()
    }
    
    private func openDatabase() {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("seats.sqlite")
        
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
    }
    
    private func closeDatabase() {
        if sqlite3_close(db) != SQLITE_OK {
            print("error closing database")
        }
    }
    
    private func createTable() {
        let createTableString = """
        CREATE TABLE IF NOT EXISTS seat (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            row INTEGER,
            column INTEGER,
            isSelected INTEGER
        );
        """
        
        var createTableStatement: OpaquePointer?
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("seat table created")
            } else {
                print("seat table could not be created")
            }
        } else {
            print("CREATE TABLE statement could not be prepared")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    func insertSeats(_ seats: [Seat]) {
        let insertStatementString = "INSERT INTO seat (row, column, isSelected) VALUES (?, ?, ?);"
        var insertStatement: OpaquePointer?
        
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            for seat in seats {
                sqlite3_bind_int(insertStatement, 1, Int32(seat.row))
                sqlite3_bind_int(insertStatement, 2, Int32(seat.column))
                sqlite3_bind_int(insertStatement, 3, seat.isSelected ? 1 : 0)
                
                if sqlite3_step(insertStatement) != SQLITE_DONE {
                    print("Could not insert seat into database")
                }
                sqlite3_reset(insertStatement)
            }
        } else {
            print("INSERT statement could not be prepared")
        }
        
        sqlite3_finalize(insertStatement)
    }

    func getAllSeats() -> [Seat] {
        let queryStatementString = "SELECT * FROM seat;"
        var queryStatement: OpaquePointer?
        var seats: [Seat] = []
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = Int(sqlite3_column_int(queryStatement, 0))
                let row = Int(sqlite3_column_int(queryStatement, 1))
                let column = Int(sqlite3_column_int(queryStatement, 2))
                let isSelected = sqlite3_column_int(queryStatement, 3) == 1
                seats.append(Seat(id: id, row: row, column: column, isSelected: isSelected))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return seats
    }
    
    func seatsExist() -> Bool {
        let queryStatementString = "SELECT COUNT(*) FROM seat;"
        var queryStatement: OpaquePointer?
        var seatCount: Int = 0
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                seatCount = Int(sqlite3_column_int(queryStatement, 0))
            }
        }
        
        sqlite3_finalize(queryStatement)
        return seatCount > 0
    }

    func updateSeatSelection(seatId: Int, isSelected: Bool) {
        let updateStatementString = "UPDATE seat SET isSelected = ? WHERE id = ?;"
        var updateStatement: OpaquePointer?
        
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(updateStatement, 1, isSelected ? 1 : 0)
            sqlite3_bind_int(updateStatement, 2, Int32(seatId))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated seat with ID \(seatId)")
            } else {
                print("Could not update seat with ID \(seatId)")
            }
        } else {
            print("UPDATE statement could not be prepared")
        }
        
        sqlite3_finalize(updateStatement)
    }
}
